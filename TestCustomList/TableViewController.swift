//
//  TableViewController.swift
//  TestCustomList
//
//  Created by Korakod Saraboon on 6/8/2559 BE.
//  Copyright © 2559 Korakod Saraboon. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class TableViewController: UITableViewController  {
    let cellIdentify = "Cell"
    
    var playlistname:String = String()
    var videolist:[String] = [String]()
    var videotitle:[String] = [String]()
    var videothumbnails:[String] = [String]()
    var row:Int = Int()
    
    // live part
    var videoLive = Bool()
    var oneTime = false
    var liveRow = Int()
    // section
    var sectionlive = ["Live video now!!" , "Video Playlist"]
    var section = ["Video Playlist"]
    
    var player:AVPlayer = AVPlayer()
    var selectedCellIndexPath: NSIndexPath?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(videoLive == true){
            
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if(videoLive == true){
            return sectionlive.count
        }else {
            return section.count
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(videoLive == true){
            return self.sectionlive[section]
        }else {
            return self.section[section]
        }
        
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if(videoLive == true ){
            if(section == 0){
                return liveRow
            }else {
                return row - liveRow
            }
        }else{
            return row
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:TableViewCell1 = self.tableView.dequeueReusableCellWithIdentifier(cellIdentify) as! TableViewCell1
        //print(indexPath)
        for sub in cell.subviews{
            if( indexPath.row != selectedCellIndexPath?.row && sub.tag == 10  ){
                //print(indexPath)
               sub.hidden = true
                if (indexPath.row > selectedCellIndexPath!.row + 3 ||  indexPath.row < selectedCellIndexPath!.row - 3){
                    player.pause()
                }
            }else if(indexPath == selectedCellIndexPath && sub.tag == 10) {
                sub.hidden = false
                print("Test out condition \(sub)")
                
                
                
                
                /// JustchangeCode alittle bit kub 555555
                // add more comment line Ja
                // ok now just test new branch
                // What the fuck
                
            }
           
        }
        
        if(videoLive == true && indexPath.section == 1){
             cell.backgroundColor = UIColor.lightGrayColor()
            cell.myLabel.text = videotitle[indexPath.row+liveRow]
            cell.myLabel.textColor = UIColor.blackColor()
            //print(videothumbnails[indexPath.row+liveRow])
            
            if let urlImg = NSURL(string: videothumbnails[indexPath.row+liveRow]),
                data = NSData(contentsOfURL: urlImg)
            {
                cell.myImage.image = UIImage(data: data)
                cell.myImage.contentMode = UIViewContentMode.ScaleAspectFit
                cell.myImage.layer.shadowOffset = CGSize(width: 3 , height: 3)
                cell.myImage.layer.shadowOpacity = 0.7
                cell.myImage.layer.shadowRadius = 2
            }
            cell.myEffect.layer.shadowOffset = CGSize(width: 3 , height: 3)
            cell.myEffect.layer.shadowOpacity = 0.3
            cell.myEffect.layer.shadowRadius = 2
            
        }else{
             cell.backgroundColor = UIColor.lightGrayColor()
            cell.myLabel.text = videotitle[indexPath.row]
            if(videoLive == true){
                cell.myLabel.textColor = UIColor.redColor()
            }
           // print(videothumbnails[indexPath.row])
            
            if let urlImg = NSURL(string: videothumbnails[indexPath.row]),
                data = NSData(contentsOfURL: urlImg)
            {
                cell.myImage.image = UIImage(data: data)
                cell.myImage.contentMode = UIViewContentMode.ScaleAspectFit
                cell.myImage.layer.shadowOffset = CGSize(width: 3 , height: 3)
                cell.myImage.layer.shadowOpacity = 0.7
                cell.myImage.layer.shadowRadius = 2
            }
            
            cell.myEffect.layer.shadowOffset = CGSize(width: 3 , height: 3)
            cell.myEffect.layer.shadowOpacity = 0.3
            cell.myEffect.layer.shadowRadius = 2
        }
        
      
    
        
        return cell
    }
    
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)  {
        selectedCellIndexPath = indexPath
        let cell:TableViewCell1 = tableView.cellForRowAtIndexPath(indexPath) as! TableViewCell1
        
         //let vc : ViewController = self.storyboard!.instantiateViewControllerWithIdentifier("OO") as! ViewController
        // vc.url = videolist[indexPath.row]
        // if(videoLive == true && indexPath.row == 0){
        //     vc.isLive = true
        // }
        // vc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        //self.presentViewController(vc, animated: true, completion: nil)
        
        //print("test inside ---\(self.url)")
        if(cell.selet == false){
            cell.selet = true
        var dict = HCYoutubeParser.h264videosWithYoutubeID(videolist[indexPath.row] as String)
        //print(dict)
            if(player.rate != 0 && player.error == nil){
                player.rate = 0
            }
         player = AVPlayer()
        if(videoLive == true && indexPath.section == 0){
            dict = HCYoutubeParser.h264videosWithYoutubeID(videolist[indexPath.row] as String)
            player = AVPlayer(URL: (NSURL(string:dict["live"] as! String))!)
        }else if(videoLive == true && indexPath.section == 1){
            dict = HCYoutubeParser.h264videosWithYoutubeID(videolist[indexPath.row+liveRow] as String)
            player = AVPlayer(URL: (NSURL(string:dict["hd720"] as! String))!)
        }else if(videoLive == false){
            player = AVPlayer(URL: (NSURL(string:dict["hd720"] as! String))!)
            }
        
        let playerController = AVPlayerViewController()
        
        playerController.player = player
        // self.presentViewController(TableViewController, animated: true, completion: nil)
        self.addChildViewController(playerController)
            playerController.view.tag = 10
        cell.addSubview(playerController.view)
        
            tableView.beginUpdates()
            tableView.endUpdates()
            playerController.view.frame = cell.contentView.frame
            if selectedCellIndexPath != nil {
                // This ensures, that the cell is fully visible once expanded
                tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .None, animated: true)
            }
        player.play()
           
        }
    }
    
    
    /**
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     if (segue.identifier == "DoubleO") {
     
     // initialize new view controller and cast it as your view controller
     let viewController = segue.destinationViewController as! ViewController
     // your new view controller should have property that will store passed value
     viewController.url = dummy
     
     }
     
     }
     **/
    
    // In this case I returning 140.0. You can change this value depending of your cell
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if(indexPath.row < liveRow && videoLive == true && indexPath.section == 0){
            return 300.0
        }else {
            if selectedCellIndexPath == indexPath {
                return 330.0
            }else{
                return 140.0
            }
        }
        
    }
    
    
    
}
