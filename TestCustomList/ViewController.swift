//
//  ViewController.swift
//  TestCustomList
//
//  Created by Korakod Saraboon on 6/8/2559 BE.
//  Copyright © 2559 Korakod Saraboon. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import youtube_parser
class ViewController: UIViewController {
    
    var url:NSString = NSString()
    var isLive = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()

      
            
            
            //print("test inside ---\(self.url)")
            let dict = HCYoutubeParser.h264videosWithYoutubeID(self.url as String)
            //print(dict)
        
            var player = AVPlayer()
            if(isLive == true){
            player = AVPlayer(URL: (NSURL(string:dict["live"] as! String))!)
            }else{
            player = AVPlayer(URL: (NSURL(string:dict["hd720"] as! String))!)
            }
        
            let playerController = AVPlayerViewController()
        
            playerController.player = player
           // self.presentViewController(TableViewController, animated: true, completion: nil)
            self.addChildViewController(playerController)
            self.view.addSubview(playerController.view)
            playerController.view.frame = self.view.frame
            player.play()
            
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

