//
//  VideoModel.swift
//  TestCustomList
//
//  Created by Korakod Saraboon on 6/8/2559 BE.
//  Copyright © 2559 Korakod Saraboon. All rights reserved.
//

import Foundation

import UIKit
import Alamofire
import youtube_parser
import SwiftyJSON

public func getFeedVideos(completionHandler:(responseObject: String?,error:String?) -> ()){
    
    let api = "AIzaSyCcXrboHom4nk_2rFMHTZPl5dttZnAh38k"
    let playlistID = "PLPZ7h6L6LC7XxhPJVsHFTXHIqxgCjNCYG"
    var reID:String = "non"
    Alamofire.request(.GET,"https://www.googleapis.com/youtube/v3/playlistItems",
        parameters: ["part":"snippet","playlistId":playlistID,"key":api,"maxResults":50],
        encoding: ParameterEncoding.URL, headers: nil).responseJSON{ response in
            
            
            if response.result.value != nil{
                let value = response.result.value
                let json = JSON(value!)
                
                //print(json["items"][0]["snippet"]["resourceId"]["videoId"])
                //print("testttttt 1 round only")
                reID = json["items"][0]["snippet"]["resourceId"]["videoId"].stringValue
               
                //let array = json["items"].array
                //print("Testing array --- >> \(array!.count)")
                print(json["items"])
                
                completionHandler(responseObject: reID ,error:"errorja")
            }
    }
}

public func getVideoList(playlistid:String,completionHandler:(videoID:Array<String>,videoTitle:Array<String>,videoThumnail:Array<String>,videoCount:Int,error:String) ->()){
    
    let api = "AIzaSyCcXrboHom4nk_2rFMHTZPl5dttZnAh38k"
    
    Alamofire.request(.GET,"https://www.googleapis.com/youtube/v3/playlistItems",
        parameters: ["part":"snippet","playlistId":playlistid,"key":api,"maxResults":50],
        encoding: ParameterEncoding.URL, headers: nil).responseJSON{ response in
            
            
            if response.result.value != nil{
                let value = response.result.value
                let json = JSON(value!)
                
                let loop:Int = json["items"].count // array count
                var videoArray = Array<String>() // video array
                var titleArray = Array<String>()  // video title array
                var thumbnailsArray = Array<String>() // video thumbnails array
                
                var item = json["items"].array
                
                // assign the video id as string to array
                for i in 0..<loop  {
                    videoArray += [item![i]["snippet"]["resourceId"]["videoId"].stringValue]
                }
                
                for o in 0..<loop{
                    titleArray += [item![o]["snippet"]["title"].stringValue]
                }
               
                for k in 0..<loop{
                    thumbnailsArray += [item![k]["snippet"]["thumbnails"]["medium"]["url"].stringValue]
                }
                
             completionHandler(
               videoID: videoArray,
               videoTitle: titleArray,
               videoThumnail: thumbnailsArray,
                videoCount: loop ,
               error: "errorja")
            }
    }

}

public func getLiveVid(channelid:String,completionHandler:(result:Array<Dictionary<String,String>>,error:String) -> ()){
    
    
    let api = "AIzaSyCcXrboHom4nk_2rFMHTZPl5dttZnAh38k"
    let cid = "UCakgsb0w7QB0VHdnCc-OVEA" // nasa test live
    let cid2 = "UCvqRdlKsE5Q8mf8YXbdIJLw" // lol test live
    let cid3 = "UCtRhN0bvQBEl51VZ0QH8oqQ" // not live
    Alamofire.request(.GET,"https://www.googleapis.com/youtube/v3/search",
        parameters: ["part":"snippet","type":"video","eventType":"live","channelId":channelid,"key":api],
        encoding: ParameterEncoding.URL, headers: nil).responseJSON{ response in
            
            
            if response.result.value != nil{
                let value = response.result.value
                let json = JSON(value!)
                
                var item = json["items"].array
                
                var arrayLive:Array<Dictionary<String,String>> = Array<Dictionary<String,String>>()
                var liveType = [String:String]()
                
               // print(item[0])
                if(item!.count > 0){
                    for i in 0..<item!.count {
                liveType["title"] = item![i]["snippet"]["title"].stringValue
                liveType["id"] = item![i]["id"]["videoId"].stringValue
                liveType["thumbnails"] = item![i]["snippet"]["thumbnails"]["medium"]["url"].stringValue
                        arrayLive += [liveType]
                    }
                
   
               // print("name = \(liveType["title"])")
               // print("id = \(liveType["id"])")
                //print("thumbnails = \(liveType["thumbnails"])")
                
                completionHandler(result:arrayLive,error: "error")
                }else{
                
                completionHandler(result:arrayLive,error: "error")
                }
            }
    }

}

public func getYouCh(){
    let api = "AIzaSyCcXrboHom4nk_2rFMHTZPl5dttZnAh38k"
    let user = "LoLChampSeries"
    Alamofire.request(.GET,"https://www.googleapis.com/youtube/v3/channels",
        parameters: ["part":"contentDetails","forUsername":user,"key":api,"maxResults":50],
        encoding: ParameterEncoding.URL, headers: nil).responseJSON{ response in
            
            
            if response.result.value != nil{
                let value = response.result.value
                let json = JSON(value!)
                
                print(json)
            }
    }

}

