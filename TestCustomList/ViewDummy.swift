//
//  ViewDummy.swift
//
//
//  Created by Korakod Saraboon on 6/9/2559 BE.
//
//

import UIKit

class ViewDummy: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var tField: UITextField!
    
    @IBOutlet weak var button: UIButton!
    
    var playlist:String = "PLV8pZFE9oHhrLW1PjfZXUBnE9kFmIvVqN"
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "Please input your playlist url"
        button.setTitle("Let's see", forState: UIControlState.Normal)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Actions
    
    
    
    @IBAction func Button(sender: UIButton) {
        
        if(tField.text != ""){
            playlist = tField.text!
            
        }
        getVideoList(playlist){ (videoID,videoTitle,videoThumnail,videoCount,error) in
            getLiveVid("UCvqRdlKsE5Q8mf8YXbdIJLw"){ (result,error) in
                
                let vc : TableViewController = self.storyboard!.instantiateViewControllerWithIdentifier("tablevideolist") as! TableViewController
                
                vc.row = videoCount
                vc.videolist = videoID
                vc.videotitle = videoTitle
                vc.videothumbnails = videoThumnail
                
                if(result.count > 0){
                    for i in 0..<result.count {
                        let str =  "Live!! " + result[i]["title"]!
                        print(result.count)
                        vc.videolist.insert(result[i]["id"]!,atIndex: 0)
                        vc.videotitle.insert(str, atIndex: 0)
                        vc.videothumbnails.insert(result[i]["thumbnails"]!, atIndex: 0)
                    }
                    vc.liveRow = result.count
                    vc.row = vc.videolist.count
                    vc.videoLive = true
                }
                
                
                vc.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
                self.presentViewController(vc, animated: true, completion: nil)
                
                
                
            }
        }
        
        
        //getYouCh()
    }
    
    
    
    
    
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
